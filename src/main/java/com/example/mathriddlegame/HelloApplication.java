package com.example.mathriddlegame;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        Merger merger = new Merger();
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("hello-view.fxml"));
        fxmlLoader.setControllerFactory (controllerClass -> new HelloController(merger));
        Scene scene = new Scene(fxmlLoader.load(), 398, 443);
        stage.setTitle("Riddle Game");
        stage.setScene(scene);
        stage.show();
        ServerConnectionThread serverConnectionThread = new ServerConnectionThread(merger);
        serverConnectionThread.start();
    }

    public static void main(String[] args) {
        launch();
    }
}