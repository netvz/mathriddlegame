package com.example.mathriddlegame;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class HelloController {
    String result;
    int counter = 0;

    public void setResult(String resultinio){
        result = resultinio;
    }

    public HelloController(Merger mergeringer){
        mergeringer.setHelloController(this);
    }

    @FXML
    private TextArea operationField;

    @FXML
    private TextField resultField;

    @FXML
    private Label numberOfPoints;

    @FXML
    private Label resultLabel;

    @FXML
    public Button checkButton;

    @FXML
    protected void setOperationField(String calculation) {
        operationField.setText(calculation);
    }

    @FXML
    protected void gameMechanics(){
        String resultFromPlayer = resultField.getText();
        if(resultFromPlayer.equalsIgnoreCase(result)){
            counter++;
            numberOfPoints.setText(String.valueOf(counter));
            resultLabel.setText("Correct!");
            resultField.setText("");
            checkButton.setDisable(true);
        }else{
            resultLabel.setText("Wrong!");
        }
    }

}