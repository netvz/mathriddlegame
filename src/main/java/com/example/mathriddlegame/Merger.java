package com.example.mathriddlegame;

import javafx.application.Platform;

public class Merger {

    HelloController helloController;

    public void setHelloController(HelloController helloControlleringer){
        helloController = helloControlleringer;
    }

    public void callSetOperationField(String calculation){
        Platform.runLater (()->helloController.setOperationField (calculation));
    }

    public void callSetResult(String result){
        Platform.runLater (()->helloController.setResult (result));
    }
}
