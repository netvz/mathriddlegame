package com.example.mathriddlegame;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;



public class ServerConnectionThread extends Thread {
    Merger merger;

    public ServerConnectionThread(Merger mergerinto){
        merger = mergerinto;
    }

    public void run() {
        BufferedReader inputFromServer;
        try {
            Socket socket = new Socket("localhost", 4200);
            inputFromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String input = "";
            String calculation = "";
            String result = "";
            do{
                merger.helloController.checkButton.setDisable(false);
                input = inputFromServer.readLine();
                String[] partedInput = input.split("=");
                calculation = partedInput[0];
                result = partedInput[1];
                merger.callSetOperationField(calculation);
                merger.callSetResult(result);
            }while(input != null);

        } catch (
                IOException e) {
            e.printStackTrace();
        }
    }
}
