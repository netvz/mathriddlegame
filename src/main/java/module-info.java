module com.example.mathriddlegame {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.mathriddlegame to javafx.fxml;
    exports com.example.mathriddlegame;
}